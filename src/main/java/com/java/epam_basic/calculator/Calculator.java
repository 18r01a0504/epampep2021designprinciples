package com.java.epam_basic.calculator;
import java.text.DecimalFormat;

public class Calculator
{
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public double add(double a, double b)
    {
        return a+b;
    }
    public double subtract(double a, double b)
    {
        return a-b;
    }
    public double multiply(double a, double b)
    {
        return Double.parseDouble(df.format(a * b));
    }
    public double divide(double a, double b)
    {
        try{

            DecimalFormat df = new DecimalFormat("00.00");
            return Double.parseDouble(df.format(a/b));
        }
        catch(Exception e){
            if(a>0)
                return Double.POSITIVE_INFINITY;
            else
                return Double.NEGATIVE_INFINITY;
        }
    }
}